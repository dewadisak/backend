
const request = require("supertest");
const app = require('../index');




describe("Todos API", () => {
  it("Post /api/add-product --> create todo", async () => {
   

    const res = await request(app).post('/api/add-product').send({
      name:'test',
      stock:12,
      price:170,

    })
    expect(res.body).toHaveProperty('stock');
    expect(res.body).toHaveProperty('stock');
    expect(res.body).toHaveProperty('price');
    expect(res.statusCode).toEqual(200)



  });

    it("GET /api -->  get by ID", async() => {

      const res = await request(app).get("/api/read-product/62206479f8d44320a201cd5a");
      expect(res.body).toEqual({
        _id: "62206479f8d44320a201cd5a",
        name: "A",
        stock: 5,
        price: 47,
        createdAt: "2022-03-03T06:47:21.683Z",
        updatedAt: "2022-03-03T06:47:21.683Z",
        __v: 0

      })


    
  });


  it("GET /api -->  status code", async() => {

    const res = await request(app).get("/api");
    expect(res.statusCode).toEqual(200)

   
   

    


  
});




});
