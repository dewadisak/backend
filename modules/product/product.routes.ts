import * as express from 'express'

// const express= require('express');
// const app = express();
const productRoute= express.Router();
let Product = require('./model/product');


//Add product
productRoute.route('/add-product').post((req:express.Request, res:express.Response, next:express.NextFunction) => {
    Product.create(req.body, (error:any, data:any) => {
        if(error){
            return next(error);

        }else{
            res.json(data);
        }
    })
})

//Get all book

productRoute.route('/').get((req:express.Request, res:express.Response, next:express.NextFunction) => {
    Product.find((error:any, data:any) =>{
        if(error){
            return next(error);

        }else{
            res.json(data);
        }
    })
})

//Get book

productRoute.route('/read-product/:id').get((req:express.Request, res:express.Response, next:express.NextFunction) => {
    Product.findById(req.params.id, (error:any, data:any) =>{
        if(error){
            return next(error);

        }else{
            res.json(data);
        }
    })
})

//Update Product

productRoute.route('/update-product/:id').put((req:express.Request, res:express.Response, next:express.NextFunction) => {
    Product.findByIdAndUpdate(req.params.id, {
        $set: req.body
    },(error:any,data:any) => {
        if(error){
            return next(error);
        

        }else{
            res.json(data);
            console.log('Product Updated');
        }

    }
    )
})


//Delete Product

productRoute.route('/delete-product/:id').delete((req:any, res:any, next:any) => {
    Product.findByIdAndRemove(req.params.id, (error:any, data:any) =>{
        if(error){
            return next(error);

        }else{
            res.status(200).json({
                msg: data
            })
        }
    })
})

module.exports = productRoute;