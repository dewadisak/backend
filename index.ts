
let express= require('express'),
path = require('path'),
mongoose  = require('mongoose'),
cors = require('cors'),
bodyParser = require('body-parser'),
mongoDb = require('./modules/product/model/db')

  mongoose.Promise = global.Promise;
  mongoose.connect( mongoDb.db, {
      useNewUrlParser:true,
      useUnifiedTopology:true
  }).then(() => {
      console.log('Database connected')
  })



  const productRoute = require('./modules/product/product.routes');
  const app  = express();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
      extended:false
  }));

  app.use(cors());


  //API Root

  app.use('/api',productRoute);

  //Port

  const port = process.env.PORT || 8000;

  app.listen(port,() =>[
      console.log('Listening on port ' + port)
  ])


  module.exports = app
